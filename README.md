# Mimimi mock telegram bot

A telegarm bot written in python using python-telegram-bot wrapper to turn all messages you sent it into i.


# How to run

- Create virtual environment

```console
python -m venv venv
source venv/bin/activate
```

- Install dependencies

```console
pip install -r requirements.txt
```

- Add environment variables:

```console
cp template.env .env
```

- Fill the environment variables

- Run main bot file

```console
python -m bot
```

# License

Read the [License](./LICENSE).
