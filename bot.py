import logging
import os
import random
import re
import uuid
from collections.abc import Callable

import dotenv
import sentry_sdk
from telegram import InlineQueryResultPhoto
from telegram import Update
from telegram.constants import ChatAction
from telegram.constants import ParseMode
from telegram.ext import ApplicationBuilder
from telegram.ext import ContextTypes
from telegram.ext import filters
from telegram.ext import InlineQueryHandler
from telegram.ext import MessageHandler

dotenv.load_dotenv()


TELEGRAM_API_TOKEN = os.getenv("TELEGRAM_API_TOKEN")
SENTRY_DSN = os.getenv("SENTRY_DSN")
REPLY_IMAGES = os.environ.get("REPLY_IMG_URLS", "")
SPONGEBOB_IMAGE = os.environ.get("SPONGEBOB_IMAGE", "")
WEBHOOK_URL = os.getenv("WEBHOOK_URL")
PORT = int(os.getenv("PORT", "8443"))
ENV = os.getenv("ENV")


def turn2i(message: str) -> str:
    return re.sub("|".join("aeiouàèìòùáéíóúïüAEIOUÀÈÌÒÙÁÉÍÓÚÏÜ"), "i", message.lower())


def turn2weird_capit(message: str) -> str:
    return "".join(letter.upper() if idx % 2 else letter.lower() for idx, letter in enumerate(message))


def format_message(callback: Callable[[str], str] = turn2i, message: str = "") -> str:
    if not message:
        message = "mimimimimi"

    return f"<i>{callback(message)}</i>"


async def mock_you(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await context.bot.send_chat_action(chat_id=update.effective_chat.id, action=ChatAction.TYPING)

    all_pics = [*REPLY_IMAGES.split(","), SPONGEBOB_IMAGE]
    callbacks = [turn2i, turn2weird_capit]

    await context.bot.send_photo(
        photo=random.choice(all_pics),
        chat_id=update.effective_chat.id,
        caption=format_message(random.choice(callbacks), update.message.text),
        parse_mode=ParseMode.HTML,
    )


def inline_query_result(photo_url: str, caption: str) -> InlineQueryResultPhoto:
    return InlineQueryResultPhoto(
        id=str(uuid.uuid4()),
        photo_url=photo_url,
        thumbnail_url=photo_url,
        title=caption,
        caption=caption,
        parse_mode=ParseMode.HTML,
    )


async def inline_mode(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await update.inline_query.answer(
        [
            inline_query_result(photo_url, format_message(turn2i, update.inline_query.query))
            for photo_url in REPLY_IMAGES.split(",")
        ]
        + [inline_query_result(SPONGEBOB_IMAGE, format_message(turn2weird_capit, update.inline_query.query))]
    )


def start_bot():
    bot = ApplicationBuilder().token(TELEGRAM_API_TOKEN).build()

    bot.add_handler(MessageHandler(filters.TEXT, mock_you))
    bot.add_handler(InlineQueryHandler(inline_mode))

    logging.info("Starting in local polling mdoe")

    bot.run_polling()


def setup_logging() -> None:
    logging.basicConfig(
        level=logging.INFO,
        style="{",
        format="{name} -- [{levelname}] {asctime}: {message}",
    )


def setup_sentry() -> None:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        max_breadcrumbs=25,
        environment="Production",
        traces_sample_rate=0.0,
    )


def main() -> int:
    setup_logging()
    setup_sentry()
    start_bot()

    return 0


if __name__ == "__main__":
    raise SystemExit(main())
