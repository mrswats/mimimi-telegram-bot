FROM python:3.11-alpine

WORKDIR /opt

ENV PATH=/opt/venv/bin:$PATH

COPY requirements.txt .

RUN : && \
    python -m venv venv && \
    python -m pip install -r requirements.txt && \
    :

COPY bot.py .

CMD ["python", "-m", "bot"]
